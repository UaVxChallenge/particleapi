package com.src;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.src.examples.Circle;
import com.src.examples.Planet;
import com.src.examples.Ring;

public class ParticleAPI extends JavaPlugin{

	public static Plugin pl;
	public static final ParticleManager api = new ParticleManager();
	
	public void onEnable(){
		pl = this;
		
		getServer().getPluginManager().registerEvents(new Circle(), this);
		getServer().getPluginManager().registerEvents(new Ring(), this);
		getServer().getPluginManager().registerEvents(new Planet(), this);

	}
	
	
}

