package com.src.examples;

import net.minecraft.server.v1_8_R3.EnumParticle;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.src.ParticleAPI;
import com.src.utils.Vectors;

public class Circle implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		
		if(e.getPlayer().getItemInHand().getType().equals(Material.ARROW)){
	
					sendParticleEffect(e.getPlayer());

			
		}
	}
	
	public void sendParticleEffect(final Player p){

					for(int i = 1; i < 1000; i++){
			
						Vector v = Vectors.getRandomVector().multiply(2);
						
						Location la = p.getLocation().add(v);
						
						
						ParticleAPI.api.sendPartcle(EnumParticle.REDSTONE, p, la.add(0,  1, 0), 0f, 0f, 0f, 0f, 10);
					
						
					}
			
		
	}
	
	
}
