package com.src.examples;

import net.minecraft.server.v1_8_R3.EnumParticle;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.src.ParticleAPI;

public class Ring implements Listener{

	
	@EventHandler
	public void onJoin(PlayerInteractEvent e){
		
		if(e.getPlayer().getItemInHand().getType().equals(Material.APPLE)){

					sendParticleEffect(e.getPlayer());
			
		}
			
		
	}
	
	public void sendParticleEffect(final Player p){

				for(int i = 1; i < 360; i++){
					
					Location la = p.getLocation();
					
					la.setX(la.getX() + Math.cos(i) * 2);
					la.setZ(la.getZ() + Math.sin(i) * 2);
					ParticleAPI.api.sendPartcle(EnumParticle.CRIT_MAGIC, p, la.add(0,  1, 0), 0f, 0f, 0f, 0f, 10);

				}
		
	
	}
	
}
