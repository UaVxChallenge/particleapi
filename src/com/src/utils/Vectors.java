package com.src.utils;

import java.util.Random;

import org.bukkit.util.Vector;

public class Vectors {

	public static Random random = new Random();
	
	public static Vector getRandomVector() {
		double x, y, z;
		x = random.nextDouble() * 2 - 1;
		y = random.nextDouble() * 2 - 1;
		z = random.nextDouble() * 2 - 1;

		return new Vector(x, y, z).normalize();
	}
	
}
